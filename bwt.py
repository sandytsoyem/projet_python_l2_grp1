
from tkinter import *

#mafenetre = Tk()
#mafenetre.title("Burrows Wheeler Transform")
#mafenetre.minsize(1000,800)
#mafenetre.mainloop()


#fonction qui effectue un décalage sur un mot ci-dessous
def decalage(mot):
    L=[]
    i=1
    t=len(mot)
    while(i<=t-1):
        L.insert(i,mot[i-1])
        i=i+1
    L.insert(0,mot[t-1])
    return L


#fonction qui effectue la transformation de BW et renvoie sa matrice ci-dessous
def transform(mo):
    T=[]
    t=len(mo)
    d=mo
    for i in range(1,t):
        d=decalage(d)
        T.insert(i,d)
    T.insert(0,transform_mot_table(mo))
    return T


#fonction qui compare deux mots et renvoie le plus petit ci-dessous
def compare_word(item1,item2):
    taille = len(item1)
    p = item1
    for i in range(taille):
        if item1[i] < item2[i]:
            p=item1
            break
        elif item1[i] > item2[i]:
            p=item2
            break
    return p


#fonction qui transforme une chaîne de caractères en tableau de caractères ci-dessous
def transform_mot_table(w):
    e = []
    t=len(w)
    for i in range(0, t):
        e.insert(i, w[i])
    return e


#fonction qui recherche un tableau de caractères dans une matrice et renvoie le numéro de sa position ci-dessous
def recherche_mot_dans_liste(mot,liste):
    p=transform_mot_table(mot)
    code = 1
    for i in range(len(liste)):
        if (p == (liste[i])):
            code=i+1
            break
    return code


#fonction qui compare un mot avec d'autres mots se trouvant dans un tableau et renvoie le mot le plus petit ci-dessous
def compare_mot_liste(mot,liste):
    t = len(liste)
    d = mot
    for i in range(0,t):
        if( d > compare_word(d,liste[i]) ):
            d = compare_word(d,liste[i])
    return d


#fonction qui renvoie la matrice rangée ci-dessous
def ranger_tout(case):
    ran=[]
    t=len(case)
    for i in range(t):
        ran.insert(i,compare_mot_liste(case[0],case))
        del(case[recherche_mot_dans_liste(compare_mot_liste(case[0],case),case)-1])
    return ran


#fonction qui renvoie la colonne à retenir ci-dessous
def colonne_a_retenir(matrice):
    col=[]
    t=len(matrice)
    for i in range(t):
        k = matrice[i]
        c = len(k)
        col.insert(i,k[c-1])
    return col

#fonction qui range un tableau de caractères dans l'ordre alphabétique
def range_mot(liste):
    t=len(liste)
    M = liste
    for i in range(0,t):
        j = i
        for j in range(t-1):
            if M[j] > M[j+1]:
                a = M[j]
                M[j] = M[j+1]
                M[j+1] = a

    return M

#fonction qui range la colonne stockée et celle-ci rangée dans l'ordre alphabétique
def tableau_des_éléments_pour_bwti(liste,liste2):
    mf=[]
    mf.insert(1,liste2)
    mf.insert(0,liste)

    return mf


def rechercheLettre(lettre,liste):
    code = 0
    for i in range(len(liste)):
        if (lettre == (liste[i])):
            code = i + 1
            break
    return code


def bwit(liste,numcol,t):
    mot=""
    numcol=numcol-1
    for i in range(t):
        mot=mot+liste[1][numcol]
        numcol=rechercheLettre(liste[1][numcol],liste[0])
    return mot


mot = "texte"
print(transform(mot))
print(ranger_tout(transform(mot)))
print(recherche_mot_dans_liste(transform_mot_table(mot),ranger_tout(transform(mot))))
print(colonne_a_retenir(ranger_tout(transform(mot))))
print(range_mot(colonne_a_retenir(ranger_tout(transform(mot)))))
print(tableau_des_éléments_pour_bwti(colonne_a_retenir(ranger_tout(transform(mot))),range_mot(colonne_a_retenir(ranger_tout(transform(mot))))))
liste=tableau_des_éléments_pour_bwti(colonne_a_retenir(ranger_tout(transform(mot))),range_mot(colonne_a_retenir(ranger_tout(transform(mot)))))
numcol=recherche_mot_dans_liste(transform_mot_table(mot),ranger_tout(transform(mot)))
t=len(colonne_a_retenir(ranger_tout(transform(mot))))
print(transform_mot_table(bwit(liste,numcol,t)))
#print(ranger_tout(transform(mot)))

